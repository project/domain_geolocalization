<?php


/**
 * @file
 * Provides internal Domain GeoLocalization functions.
 */


/**
 * Store a user's (or session's) location data.
 */
function _domain_geolocalization_update_user(&$edit, &$account) {
  $edit['domain_geolocalization'] = $account->domain_geolocalization = domain_geolocalization_set_user_location($edit['domain_geolocalization']);
}

/**
 * Retrieve user's or session's location data.
 */
function _domain_geolocalization_load_user_location($account) {
  if ($account->uid > 0) {
    $field = 'uid';
    $value = $account->uid;
  }
  elseif (module_exists('domain_session_default') && session_api_available()) {
    $field = 'sid';
    $value = session_api_get_sid();
  }

  if ($field) {
    $result = db_query("SELECT * FROM {domain_geolocalization_location_instance} LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE " . $field . " = %d", $value);
    $location = db_fetch_array($result);
    // transform to a pure location
    unset($location['domain_id'], $location['uid'], $location['sid']);
    return $location;
  }

  return FALSE;
}

/**
 * Build the search block.
 */
function _domain_geolocalization_search_block() {
  $form = drupal_get_form('domain_geolocalization_search_form');
  return theme('domain_geolocalization_search_block', $form);
}

/**
 * Build the international domain selector.
 */
function _domain_geolocalization_international_domain_selector() {
  $form = drupal_get_form('domain_geolocalization_international_domain_selector_form');
  return theme('domain_geolocalization_international_domain_selector', $form);
}

/**
 * Javascript wrapper function for domain_geolocalization_search_form to
 * return the form as json. This will only be called if the Domain User
 * Default module is installed.
 */
function domain_geolocalization_search_form_js() {
  $form = drupal_get_form('domain_geolocalization_search_form');

  // add js to hide link that called this
  drupal_add_js('$("#domain-user-default-change").hide("slow");', 'inline');

  if (variable_get('domain_geolocalization_hover_search_form', TRUE)) {
    // add js to add a close link. And alter the behavior of the
    // Change link. Note, this must go outside of #domain-search,
    // since that element is entirely replaced via this callback.
    // @fixme: The click function attached to
    // domaine-user-default-change is called, but the AHAH processing
    // is still called as well, replacing the form with a fresh
    // version.
    drupal_add_js('$("#domain-search-close").val("' . t('Close') . '").show().click(function() { $(this).hide(); $("#domain-search-wrapper").hide("slow"); $("#domain-user-default-change").removeClass("ahah-processed").click(function() {$("#domain-search-wrapper").show("slow"); return false;}).show(); return false;});', 'inline');

    // add style to float div
    $style = '<link type="text/css" media="screen" rel="stylesheet" href="' . base_path() . drupal_get_path('module', 'domain_geolocalization') . '/domain_geolocalization.form.css' . '" />';
  }

  $script = drupal_get_js();
  return drupal_json(array('status' => TRUE, 'data' => $script . $form . $style));
}

/**
 * Search form.
 */
function domain_geolocalization_search_form($form_state) {

  $form['postalcode'] = array(
    '#type' => 'textfield',
    '#title' => t('U.S. Postal Code'),
    '#required' => TRUE,
    '#default_value' => $values['postalcode'],
    '#size' => 15,
  );

  if (!variable_get('domain_geolocalization_use_domain_radius', 0)) {
    // attach search radius parameter
    $form['proximity'] = array(
      '#type' => 'select',
      '#title' => t('Search radius'),
      '#options' => array(
        10 => t('10 miles'),
        20 => t('20 miles'),
        50 => t('50 miles'),
        100 => t('100 miles'),
        500 => t('500 miles'),
        1000 => t('1000 miles'),
      ),
      '#required' => TRUE,
      '#default_value' => $values['proximity'] ? $values['proximity'] : 50,
    );
  }

  // link to international domains
  $form['all'] = array(
    '#type' => 'markup',
    '#value' => '<div>' . l(t('All domains'), 'domains') . '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#ahah' => array(
      'path' => 'search/domain/js',
      'wrapper' => 'domain-search-results',
      'method' => 'replace',
      'effect' => 'fade',
      // The Drupal API sets the event to 'mousedown' by default. In this case
      // we want any form submission to trigger the ahah event.
      'event' => 'click',
    ),
  );

  // add a couple of divs for the search results to land in
  $form['domain_search_wrapper'] = array(
    '#prefix' => '<div id="domain-search-results-wrapper" class="clear-block">',
    '#suffix' => '</div>',
  );
  $form['domain_search_wrapper']['search_results'] = array(
    '#value' => '<div id="domain-search-results"></div>',
  );

  return $form;
}

/**
 * Search form submit handler.
 */
function domain_geolocalization_search_form_submit(&$form, &$form_state) {
  if (!variable_get('domain_geolocalization_use_domain_radius', 0)) {
    $proximity =  '/' . trim($form_state['values']['proximity']);
  }
  $form_state['redirect'] = 'search/domain/' . trim($form_state['values']['postalcode']) . $proximity;
}

/**
 * Return a form selector for international (as defined by zipcode
 * being blank).
 */
function domain_geolocalization_international_domain_selector_form(&$form_state) {
  global $user;

  $account = user_load($user->uid);

  $form['domain_id'] = array(
    '#type' => 'select',
    '#default_value' => $account->default_domain,
    '#options' => domain_geolocalization_international_domain_options(),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  // attach dynamic behaviour
  // @todo  drupal_add_js()

  return $form;
}

/**
 * Submit handler for International Domain Selector form, simply falls
 * through to the user domain switcher.
 */
function domain_geolocalization_international_domain_selector_form_submit($form, &$form_state) {
  module_load_include('inc', 'domain_user_default');
  domain_user_default_domain_switcher_form_submit($form, $form_state);
}

/**
 * Provides and options list for use in a form of all international (Country other than US).
 */
function domain_geolocalization_international_domain_options() {
  static $options;

  if (!isset($options)) {
    // @todo make the country configurable instead of hard-coding US.
    $country = variable_get('domain_geolocalization_country_default', 'US');

    $result = db_query("SELECT * FROM {domain_geolocalization} LEFT JOIN {domain} USING (domain_id) LEFT JOIN {domain_geolocalization_location_instance} USING (domain_id) LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE valid AND country <> '%s' AND country <> ''", $country);

    $options = array(0 => t('=== Choose ==='));
    while ($domain = db_fetch_object($result)) {
      // Cannot pass zero in checkboxes.
      $key = ($domain->domain_id == 0) ? -1 : $domain->domain_id;
      $options[$key] = $domain->sitename;
    }
  }

  return $options;
}