<?php


/**
 * @file
 * Domain Geolocalization page callbacks
 */

/**
 * Menu Callback: Search Page.
 */
function domain_geolocalization_search_page() {
  $values = array(
    'postalcode' => arg(2),
    'proximity' => arg(3),
  );

  module_load_include('inc', 'domain_geolocalization');
  $form = drupal_get_form('domain_geolocalization_search_form', $values);
  if ($values['postalcode']) {
    // determine lat/long of zipcode
    module_load_include('inc', 'domain_geolocalization', 'domain_geolocalization.geocode');
    $address = _domain_geolocalization_geocode(array('zipcode' => arg(2)));
    if (!$address['zipcode']) {
      $results = t('Invalid postal code: @postal', array('@postal' => arg(2)));
    }
    else {
      $domains = domain_geolocalization_search_results(arg(2), arg(3), $address);

      if (module_exists('domain_user_default') && count($domains)) {
        // see if the domain should be automatically set
        $auto_set = variable_get('domain_user_default_geolocalization_search_result_behavior', DOMAIN_USER_DEFAULT_GEOLOCALIZATION_AUTO_SET_NEVER);
        switch ($auto_set) {
          case DOMAIN_USER_DEFAULT_GEOLOCALIZATION_AUTO_SET_SINGLE:
            if (count($domains) > 1) {
              break;
            }
          case DOMAIN_USER_DEFAULT_GEOLOCALIZATION_AUTO_SET_TOP:
            // At this point, these both behave the same
            $domain_id = $domains[0]['domain_id'];
            drupal_goto('domain-user-default/set/' . $domain_id);
            default:
        }
      }
      $results =  theme('domain_geolocalization_search_results', $domains, $address, $values['proximity'], 'miles');
    }
  }
  // @todo Integrate the Domain User Default search behavior options here, for
  // auto-setting based on results.

  return theme('domain_geolocalization_search_page', $form, $results);
}

/**
 * Menu Callback: JS search results.
 */
function domain_geolocalization_search_js() {
  $zip = $_POST['postalcode'];
  $proximity = $_POST['proximity'];

  // determine lat/long of zipcode
  module_load_include('inc', 'domain_geolocalization', 'domain_geolocalization.geocode');
  $address = _domain_geolocalization_geocode(array('zipcode' => $zip));

  if (!$address['zipcode']) {
    $results = t('Invalid postal code: @postal', array('@postal' => $zip));
  }
  else {
    $domains = domain_geolocalization_search_results($zip, $proximity, $address);

    if (module_exists('domain_user_default') && count($domains)) {
      // see if the domain should be automatically set
      $auto_set = variable_get('domain_user_default_geolocalization_search_result_behavior', DOMAIN_USER_DEFAULT_GEOLOCALIZATION_AUTO_SET_NEVER);
      switch ($auto_set) {
      case DOMAIN_USER_DEFAULT_GEOLOCALIZATION_AUTO_SET_SINGLE:
	if (count($domains) > 1) {
	  break;
	}
      case DOMAIN_USER_DEFAULT_GEOLOCALIZATION_AUTO_SET_TOP:
	// At this point, these both behave the same
	$domain_id = $domains[0]['domain_id'];
	// @todo find out if there is a way to do this within the
	// framework of the Drupal Form AHAH handling.
	$script = '<script type="text/javascript">location.href="' . url('domain-user-default/set/' . $domain_id, array('absolute' => TRUE)) . '"</script>';
	return drupal_json(array('status' => TRUE, 'data' => $script, 'set_domain' => $domain_id));
      default:
      }
    }

    $results = theme('domain_geolocalization_search_results_js', $domains, $address, $proximity, 'miles');
  }
  return drupal_json(array('status' => TRUE, 'data' => $results));
}

/**
 * Menu Callback: International domains
 *
 * A list of domains defined by a country != primary domain country
 */
function domain_geolocalization_international_domains_page() {
  // @todo Make this configurable for primary domain
  $country = 'US';

  $result = db_query("SELECT domain_id FROM {domain_geolocalization} LEFT JOIN {domain} USING (domain_id) LEFT JOIN {domain_geolocalization_location_instance} USING (domain_id) LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE valid AND country <> '%s' AND country <> ''", $country);

  $domains = array();
  while ($dom = db_fetch_object($result)) {
    $domains[] = domain_lookup($dom->domain_id);
  }

  return theme('domain_geolocalization_domain_list', $domains);
}

/**
 * Menu Callback: All domains
 *
 * @todo This should really be done via views, or by the domain
 * module. For now, it provides a list with an eye toward the location
 * aspect of each domain.
 */
function domain_geolocalization_all_domains_page() {
  // @todo make the order configurable
  $order = 'd.sitename';

  $result = db_query("SELECT domain_id FROM {domain} d LEFT JOIN {domain_geolocalization} dg USING (domain_id) LEFT JOIN {domain_geolocalization_location_instance} dgli USING (domain_id) LEFT JOIN {domain_geolocalization_location} dgl USING (dglid) WHERE valid ORDER BY " . $order);

  // add primary domain
  $domains[] = domain_lookup(0);

  while ($dom = db_fetch_object($result)) {
    $domains[] = domain_lookup($dom->domain_id);
  }

  return theme('domain_geolocalization_domain_list', $domains);
}