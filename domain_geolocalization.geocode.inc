<?php


/**
 * @file
 * Domain GeoLocalization geocoding functions. Currently using Google Maps.
 */

define('DOMAIN_GEOLOCALIZATION_GMAP_REQUEST_URL', 'http://maps.google.com/maps/geo?output=xml');

/**
 * Find domains withing proximity from lat/long.
 *
 * @param $longitude numeric
 * @param $latitude numeric
 * @param $proximity numeric
 * @param $units string default 'miles'
 * @return array list of domains
 */
function domain_geolocalization_locate_domains($longitude, $latitude, $proximity, $units = 'miles', $address) {
  // load proximity library
  module_load_include('inc', 'domain_geolocalization', 'earth');

  $earth_distance  = earth_distance_sql($longitude, $latitude);
  if (!variable_get('domain_geolocalization_use_domain_radius', 0)) {
    // the above library is written for meters, so convert miles and kms
    $proximity = _domain_geolocalization_convert_to_meters($proximity, $units);

    // user defined proximity search
    $range_latitude  = earth_latitude_range($longitude, $latitude, $proximity);
    $range_longitude = earth_longitude_range($longitude, $latitude, $proximity);

    $result = db_query('SELECT domain_id, ' . $earth_distance . ' AS distance FROM {domain_geolocalization} LEFT JOIN {domain} USING (domain_id) LEFT JOIN {domain_geolocalization_location_instance} USING (domain_id) LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE valid AND latitude > %f AND latitude < %f AND longitude > %f AND longitude < %f AND ' . $earth_distance . ' < %f ORDER BY distance', 
      $range_latitude[0], $range_latitude[1], $range_longitude[0], $range_longitude[1], $proximity);
  }
  else {
    $radius_sql = _domain_geolocalization_convert_to_meters_sql('radius', 'miles');
    // searching using domain radius for proximity
    $result = db_query('SELECT domain_id, ' . $earth_distance . ' AS distance FROM {domain_geolocalization} LEFT JOIN {domain} USING (domain_id) LEFT JOIN {domain_geolocalization_location_instance} USING (domain_id) LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE valid AND ' . $earth_distance . ' < ' . $radius_sql . ' ORDER BY distance');
  }

  $domains = array();
  while ($dom = db_fetch_object($result)) {
    $domain = domain_lookup($dom->domain_id);
    $domain['distance'] = $units == 'miles' ? _domain_geolocalization_convert_to_miles($dom->distance, 'meters') : $dom->distance;
    $domains[] = $domain;
  }

  // fallback to state level if no domains have been found
  if (empty($domains) && $address['state']) {
    if (variable_get('domain_geolocalization_use_state_defaults', FALSE)) {
      $state_default = ' AND state_default';
    }
    $result = db_query('SELECT domain_id, ' . $earth_distance . ' as distance FROM {domain_geolocalization} LEFT JOIN {domain} USING (domain_id) LEFT JOIN {domain_geolocalization_location_instance} USING (domain_id) LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE valid AND state = \'%s\''. $state_default . ' ORDER BY distance', $address['state']);
    while ($dom = db_fetch_object($result)) {
      $domain = domain_lookup($dom->domain_id);
      $domain['distance'] = $units == 'miles' ? _domain_geolocalization_convert_to_miles($dom->distance, 'meters') : $dom->distance;
      $domains[] = $domain;
    }
  }

  return $domains;
}

/**
 * Send a request to Google Maps for locative data.
 *
 * @param $address array
 *   usually only consisting of a zipcode
 * @return assoc array
 *   keyed array containing found information from the zipcode lookup
 *
 */
function _domain_geolocalization_geocode($address) {
  if (!$address['zipcode']) {
    return FALSE;
  }

  $http_reply = _domain_geolocalization_get_google_data($address);

  // find the response code (<code>{code}</code>
  preg_match('#<code>([0-9]+)</code>#', $http_reply->data, $m);
  $response_code = $m[1];

  switch ($response_code) {
    case 200:
      // a good request
      return _domain_geolocalization_parse_gmap_response($http_reply->data);
      break;
      // @todo add some useful error checking
    default:
  }
  return FALSE;
}

/**
 * Parse Google Map response.
 *
 * @param $data string
 * @return assoc array
 *   Keyed array containing locative information
 */
function _domain_geolocalization_parse_gmap_response($data) {
  if (preg_match('#<coordinates>([-.0-9]+),([-.0-9]+)(,.*)?</coordinates>#', $data, $m)) {
    $parsed['longitude'] = $m[1];
    $parsed['latitude'] = $m[2];
  }

  $the_rest = array(
    // google name => internal name
    'AdministrativeAreaName' => 'state',
    'LocalityName' => 'city',
    'PostalCodeNumber' => 'zipcode',
    'CountryNameCode' => 'country',
  );

  foreach ($the_rest as $attribute => $local) {
    // @fixme this pattern is too greedy in the event that more than
    // one record is found (international zipcodes)
    if (preg_match('#<' . $attribute . '>([-, 0-9A-Za-z]+)</' . $attribute . '>#', $data, $m)) {
      $parsed[$local] = $m[1];
    }
  }

  if (!empty($parsed)) {
    return $parsed;
  }

  return FALSE;
}

/**
 * Convert a distance to meters.
 */
function _domain_geolocalization_convert_to_meters($distance, $units) {
  switch ($units) {
  case 'miles':
    return (float)$distance * 1609.344;
  case 'km':
  case 'kilometers':
    return (float)$distance * 1000.000;
  case 'meters':
  case 'metres':
    return (float)$distance;
  default:
    return FALSE;
  }
}

/**
 * Convert a distance to miles.
 */
function _domain_geolocalization_convert_to_miles($distance, $units) {
  switch ($units) {
  case 'meters':
  case 'metres':
    return (float)$distance / 1609.344;
  case 'km':
  case 'kilometers':
    return (float)$distance / 1.609344;
  case 'miles':
    return (float)$distance;
  default:
    return FALSE;
  }
}

/**
 * SQL for unit conversion to miles
 * @param string $field_name
 * @param string $units - units the field is in
 * @return string
 *   SQL to convert $field_name from $units to meters
 */
function _domain_geolocalization_convert_to_meters_sql($field_name, $units) {
  switch ($units) {
  case 'meters':
  case 'metres':
    return $field_name;
  case 'km':
  case 'kilometers':
    return $field_name . ' * 1000.000 ';
  case 'miles':
    return $field_name . ' * 1609.344 ';
  default:
    return FALSE;
  }
}

/**
 * Perform Google request.
 */
function _domain_geolocalization_get_google_data($address) {
  global $_domain;

  // current domain requires a gmap key, or request will fail
  if (!$_domain['gmap_key']) {
    watchdog('Domain GeoLocalization GMap failure', 'No Google Maps key found for @domain', array('@domain' => $_domain['sitename']), WATCHDOG_ERROR);
    return FALSE;
  }

  $url = DOMAIN_GEOLOCALIZATION_GMAP_REQUEST_URL . '&key=' . $_domain['gmap_key'];

  $query = urlencode($address['zipcode']);

  return drupal_http_request($url . '&q=' . urlencode($query));
}
