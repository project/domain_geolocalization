<?php


/**
 * @file
 * Domain Geolocalization admin functions.
 */

/**
 * Domain edit/create form.
 */
function _domain_geolocalization_domain_edit_form(&$form, $form_state, $form_id) {
  $domain = domain_lookup($form['domain_id']['#value']);

  $form['domain_geolocalization'] = array(
    '#type' => 'fieldset',
    // set lighter than the submit button
    '#weight' => 1,
    '#title' => t('Domain GeoLocalization'),
    '#description' => t('Associate this domain with a geographic area.<br /><strong>Important</strong>: None of these corresponding fields are verified. Thus it is important that the latitude/longitude are correct for the city, state, country and zipcode entered. The only required fields are latitude and longitude.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#validate' => '_domain_geolocalization_domain_validate',
  );
  $form['domain_geolocalization']['use_geolocalization'] = array(
    '#type' => 'checkbox',
    '#default_value' => !is_null($domain['latitude']),
    '#title' => t('Use Geolocalization for this domain'),
  );
  $form['domain_geolocalization']['latitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Latitude'),
    '#default_value' => $domain['latitude'],
  );
  $form['domain_geolocalization']['longitude'] = array(
    '#type' => 'textfield',
    '#title' => t('Longitude'),
    '#default_value' => $domain['longitude'],
  );
  $form['domain_geolocalization']['radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Search Radius'),
    '#description' => t('Radius of the domain, in miles'),
    '#default_value' => $domain['radius'],
  );

  // @fixme: This should integrate with GMaps once it is ported. However, this
  // will be tricky since we are dealing with subdomains. The Keys API module
  // would be another good choice for integration. On the other hand, keeping
  // it here keeps domain adminstration all in one place.
  $form['domain_geolocalization']['gmap_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Maps API key'),
    '#description' => t('Each subdomain requires a !link', array('!link' => l('Google Maps API key', 'http://code.google.com/apis/maps/signup.html'))),
    '#default_value' => $domain['gmap_key'],
  );

  $form['domain_geolocalization']['city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#default_value' => $domain['city'],
  );

  $form['domain_geolocalization']['state'] = array(
    '#type' => 'textfield',
    '#title' => t('State Code'),
    '#default_value' => $domain['state'],
  );

  if (variable_get('domain_geolocalization_use_state_defaults', FALSE)) {
    $form['domain_geolocalization']['state_default'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set this domain as the default domain for this state'),
      '#default_value' => $domain['state_default'],
      '#description' => t('Only one default may be defined per state'),
    );
  }

  $form['domain_geolocalization']['zipcode'] = array(
    '#type' => 'textfield',
    '#length' => 30,
    '#title' => t('US Postal Code'),
    '#default_value' => $domain['zipcode'],
  );

  $form['domain_geolocalization']['country'] = array(
    '#type' => 'textfield',
    '#title' => t('Country'),
    '#default_value' => $domain['country'],
  );

  // weigh down the submit button
  $form['submit']['#weight'] = 10;

  // append custom validation and functions
  $form['#validate'][] = '_domain_geolocalization_domain_edit_validate';
}


/**
 * Hanlder for domain edit/create form validation.
 */
function _domain_geolocalization_domain_edit_validate($form, &$form_state) {
  // if use geolocation has been checked, values are required
  if ($form_state['values']['use_geolocalization']) {
    foreach (array('latitude', 'longitude', 'radius', 'gmap_key') as $key) {
      $value = $form_state['values'][$key];
      if (!$value) {
        // @fixme: this method of checking for a value disallows the value of 0 for lat or long.
        form_set_error($key, t('@field is required for Domain Geolocalization', array('@field' => $form['domain_geolocalization'][$key]['#title'])));
      }
    }

    // check for existing domains defined as default
    if (variable_get('domain_geolocalization_use_state_defaults', FALSE) && $form_state['values']['state_default']) {
      if (!$form_state['values']['state']) {
        // state must be set to check for an existing one
        form_set_error('state', t('@field is required if setting as the <em>State Default</em>', array('@field' => $form['domain_geolocalization']['state']['#title'])));
      }
      else {
        // check if there is a state default already defined for this state
	if ($form_state['values']['domain_id']) {
	  // existing domain, so the query is altered to exclude itself
	  $alter = ' AND domain_id <> %d';
	}
        $existing = db_result(db_query("SELECT domain_id FROM {domain_geolocalization} LEFT JOIN {domain_geolocalization_location_instance} USING (domain_id) LEFT JOIN {domain_geolocalization_location} USING (dglid) WHERE state = '%s' AND state_default" . $alter, $form_state['values']['state'], $form_state['values']['domain_id']));
        if ($existing) {
          $domain = domain_lookup($existing);
          form_set_error('state', t('<a href="!url">@domain</a> is already set as the default domain for @state', array('!url' => url(domain_get_path($domain)), '@state' => $form_state['values']['state'], '@domain' => $domain['sitename'])));
        }
      }
    }
  }
}
