<?php


/**
 * @file
 * Domain GeoLocalization theme functions.
 */

/**
 * Theme location data.
 */
function theme_domain_geolocalization_location_data($domain) {
  return t('Lat: @lat Long: @long Radius: @rad miles',
    array('@lat' => $domain['latitude'], '@long' => $domain['longitude'], '@rad' => $domain['radius'])
  );
}

/**
 * Theme search results.
 *
 * @param $domains array of domain arrays
 * @param $address array of address parameters
 * @param $proximity float radius of domains
 * @return string formatted search results
 */
function theme_domain_geolocalization_search_results($domains, $address, $proximity, $units) {
  $domain_radius = variable_get('domain_geolocalization_use_domain_radius', 0);

  if (empty($domains)) {
    // change text based on search method (otherwise distance comes out blank).
    $text = $domain_radius ? 'No domains found near postal code @postal.' : 'No domains found within @distance @units of postal code @postal.';
    $default = domain_lookup(0);
    return t($text . '<br /><a href="!url">List of all domains</a> | <a href="!default">Return to @name</a>', array('@distance' => $proximity, '@units' => $units, '@postal' => $address['zipcode'], '!url' => url('domains'), '!default' => url(domain_get_path($default)), '@name' => $default['sitename']));
  }

  foreach ($domains as $domain) {
    switch (variable_get('domain_geolocalization_search_display', DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_BOTH)) {
    case DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_SWITCH:
      if (module_exists('domain_user_default')) {
	// Display only a link to switch default domain.
	$list[] = round($domain['distance'], 2) . ' ' . $units . ': ' . l($domain['sitename'], 'domain-user-default/set/' . $domain['domain_id']);
	break;
      }
      // fall through if module doesn't exist
    case DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_BOTH:
      // add user default link if module exists
      if (module_exists('domain_user_default')) {
	$default_link = ' | ' . l(t('Set as default'), 'domain-user-default/set/' . $domain['domain_id']);
      }
      
      $list[] = round($domain['distance'], 2) . ' ' . $units . ': ' . l($domain['sitename'], domain_get_path($domain)) . $default_link;
      break;
    case DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_LINK:
      $list[] = round($domain['distance'], 2) . ' ' . $units . ': ' . l($domain['sitename'], domain_get_path($domain));
    }
  }

  $title_text = $domain_radius ? 'Domains found for postal code @zip' : 'Domains found within @distance @units of postal code @zip';

  return '<div class="domain"><div><strong>' . t($title_text, array('@distance' => $proximity, '@units' => $units, '@zip' => $address['zipcode'])) . '</strong></div>
  ' . theme('item_list', $list) . '
</div>
';
}

/**
 * Theme js search results. Default is simply to use normal search
 * results page theme.
 */
function theme_domain_geolocalization_search_results_js($domains, $address, $proximity, $units) {
  return theme('domain_geolocalization_search_results', $domains, $address, $proximity, $units);
}

/**
 * Theme domain search block.
 */
function theme_domain_geolocalization_search_block($form) {
  // @todo, perhaps some links to other search options.
  return $form;
}

/**
 * Theme international domain selector block.
 */
function theme_domain_geolocalization_international_domain_selector($form) {
  return $form;
}

/**
 * Theme search results page.
 */
function theme_domain_geolocalization_search_page($form, $results) {
  return $results . $form;
}

/**
 * Theme a list of domains.
 *
 * @todo This would be much better if it could be done via views, but
 * at this time, domains aren't first class views objects.
 *
 * @param array $domains - array of domains
 */
function theme_domain_geolocalization_domain_list($domains) {
  if (empty($domains)) {
    $output = t('No domains found');
  }
  else {
    foreach ($domains as $domain) {
      // reset variables used
      $country = $default_link = $link = '';

      if ($domain['country']) {
	$country = ' (' . $domain['country'] . ')';
      }

      switch (variable_get('domain_geolocalization_search_display', DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_BOTH)) {
        case DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_SWITCH:
          // add user default link if module exists
          if (module_exists('domain_user_default') && user_access('set own domain default')) {
            $list[] = l($domain['sitename'], 'domain-user-default/set/' . $domain['domain_id']) . $country;
            break;
          }
          // fall through if no domain user default module
        case DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_BOTH:
          // add user default link if module exists
          if (module_exists('domain_user_default') && user_access('set own domain default')) {
            $default_link = ' | ' . l(t('Set as default'), 'domain-user-default/set/' . $domain['domain_id']);
          }
          $list[] = l($domain['sitename'], domain_get_path($domain)) . $country . $default_link;
          break;
        case DOMAIN_GEOLOCALIZATION_SEARCH_DISPLAY_LINK:
          $list[] = l($domain['sitename'], domain_get_path($domain)) . $country;
      }
    }
    $output = theme('item_list', $list);
  }

  return $output;
}
