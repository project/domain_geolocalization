
/**
 * @file
 * README for the Domain GeoLocalization module
 */

Introduction
------------

The Domain GeoLocalization module is a sub-module of the Domain [1]
module that provides each domain with loative information (Lat/Long,
city, state, postal code, country) and methods for searching for
domains within a givin proximity. The module uses the Google Maps API
for geocoding, and as such requires a developer key for each domain [2].

For additional functionality, the Domain User Default [3] module provides
easy ways for users to set a domain as their default, once they have
located one nearby.


Sponsors
--------

The Domain Geolocalization module is sponsored by One Economy
(http://one-economy.com) and developed and maintained by OpenSourcery
(http://opensourcery.com).


Installation
------------

  1) Move the module code into your modules directory, and install

  2) Add locative information to existing/new domains at: admin/build/domain

  3) If the search module is active, a new tab will be presented to
     users when they visit the search page.

  4) The module provides permissions which must be granted in order to
     search domains.

  5) The module provides several blocks which can be enabled and
     configured at admin/build/block

  6) If the Domain User Default module is installed, the
     aforementioned blocks can be configured to work with user/session
     domain defaults.


References
----------

1. http://drupal.org/project/domain
2. http://code.google.com/apis/maps/signup.html
3. http://drupal.org/project/domain_user_default
